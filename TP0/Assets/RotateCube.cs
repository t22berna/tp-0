using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class RotateCube : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 3.0f;
    public InputAction activateRotation;
    public bool rotationEnabled = false;
    private void Awake(){
        activateRotation = new InputAction(binding:"<Keyboard>/r");
        activateRotation.Enable();
        activateRotation.started += ctx => OnRotationActivation();
    }
    private void OnRotationActivation(){
        rotationEnabled = !rotationEnabled;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(rotationEnabled){
            transform.Rotate(0,speed,0);
        }
        else{
            transform.Rotate(0,0,0);
        }
        
    }
}
