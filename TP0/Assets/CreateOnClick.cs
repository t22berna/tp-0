using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{
    public GameObject aCopier;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(1)){
            Vector3 cameraPosition = Camera.main.transform.position;
            Vector3 newcoord = cameraPosition + Camera.main.transform.forward;
            GameObject NouveauCube = Instantiate(aCopier,newcoord,Quaternion.identity);
            Rigidbody rb = NouveauCube.GetComponent<Rigidbody>();
            Renderer rd = NouveauCube.GetComponent<Renderer>();
            rd.material.color = Color.green;
            rb.AddForce(Camera.main.transform.forward*500.0f);
        }
        
    }

}
