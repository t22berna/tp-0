using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Rigidbody cubeRigidbody;
    private Renderer cubeRenderer;
    private Color initialColor;

    private void Start()
    {
        // Initialisation du Rigidbody et du Renderer
        cubeRigidbody = GetComponent<Rigidbody>();
        cubeRenderer = GetComponent<Renderer>();

        // Stocker la couleur initiale
        initialColor = cubeRenderer.material.color;
    }

    // Cette méthode est appelée lorsqu'on clique sur le cube
    public void OnPointerClick(PointerEventData eventData)
    {
        // Appliquer une force au Rigidbody pour pousser le cube
        float forceMagnitude = 50000.0f;
        cubeRigidbody.AddForce(Camera.main.transform.forward * forceMagnitude);
    }

    // Cette méthode est appelée lorsque la souris entre dans la zone du cube
    public void OnPointerEnter(PointerEventData eventData)
    {
        // Changer la couleur du cube (par exemple, en rouge)
        cubeRenderer.material.color = Color.red;
    }

    // Cette méthode est appelée lorsque la souris quitte la zone du cube
    public void OnPointerExit(PointerEventData eventData)
    {
        // Rétablir la couleur initiale du cube
        cubeRenderer.material.color = initialColor;
    }
}

